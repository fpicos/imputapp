export default {
  action: {
    back: 'Volver',
    cancel: 'Cancelar',
    confirm: 'Confirmar',
    error: 'Se ha producido un error',
    removeDialog: '¿Seguro que quieres eliminar el registro seleccionado?',
    save: 'Guardar',
    saved: 'Cambios guardados correctamente',
    select: 'Seleccione una opción | Seleccione al menos una opción'
  },
  form: {
    email: 'No es una dirección de correo electrónico válida',
    maxLength: 'Debe tener una longitud máxima de {max} caracteres',
    minLength: 'Debe tener una longitud mínima de {min} caracteres',
    required: 'El campo es obligatorio',
    sameAs: 'Los valores no coinciden'
  },
  label: {
    company: 'Empresa',
    project: 'Proyecto',
    name: 'Nombre',
    surname: 'Apellidos',
    email: 'Correo electrónico',
    password: 'Contraseña',
    is_admin: 'Permisos de administración',
    day: 'Día',
    partialDay: 'Por horas',
    description: 'Descripción',
    range: {
      start: 'Desde',
      end: 'Hasta'
    },
    user: 'Usuario',
    yes: 'Sí',
    no: 'No'
  },
  menu: {
    notify: 'Notificar una ausencia',
    profile: 'Mi perfil',
    reports: 'Mis informes',
    users: 'Gestión de empleados'
  },
  notification: {
    error: 'Problema con la ubicación',
    unlocated: 'Ubicación incorrecta',
    message: 'Por favor, inserta manualmente el informe a través de la app'
  },
  profile: {
    password: {
      confirm: 'Repetir nueva contraseña',
      next: 'Nueva contraseña',
      title: 'Cambiar contraseña'
    }
  },
  user: {
    addReport: 'Añadir informe',
    login: {
      error: 'Nombre de usuario o contraseña incorrectos',
      remember: 'Mantener sesión iniciada',
      submit: 'Iniciar sesión'
    },
    logout: 'Cerrar sesión',
    message: 'Hola, {name}',
    new: 'Nuevo usuario',
    projects: 'Proyectos asignados',
    recover: {
      link: 'No me acuerdo de mi contraseña',
      sent: 'Se ha enviado una nueva contraseña por email'
    }
  }
}

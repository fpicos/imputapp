export default {
  data: () => ({ loading: false }),
  methods: {
    confirmSubmit () {
      this.loading = true
      this.onSubmit()
        .then(this.onSubmitOk)
        .catch(this.onSubmitError)
        .finally(() => { this.loading = false })
    },
    submit () {
      if (!this.loading) {
        if (!this.$v) {
          this.confirmSubmit()
        } else {
          this.$v.$touch()
          if (!this.$v.$error) {
            this.confirmSubmit()
          }
        }
      }
    },
    onSubmitOk () {
      this.$q.notify({
        type: 'positive',
        message: this.$t('action.saved')
      })
    },
    onSubmitError () {
      this.$q.notify({
        type: 'negative',
        message: this.$t('action.error')
      })
    }
  }
}

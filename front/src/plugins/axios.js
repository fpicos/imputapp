import Axios from 'axios'

const IP_ADDRESS = '192.168.1.39'

export const axios = Axios.create({
  baseURL: (process.env.DEV ? `http://${IP_ADDRESS}:8000/` : 'http://imputapp.mucocu.net/')
})

export default ({ Vue }) => {
  Vue.prototype.$axios = axios
}

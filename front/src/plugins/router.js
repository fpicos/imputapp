import { Loading } from 'quasar'
import { PAGE } from 'src/router/routes'

export default ({ router, store }) => {
  router.beforeResolve(async (to, from, next) => {
    Loading.show()

    const isAuthPage = [PAGE.login, PAGE.recover].includes(to.name)

    try {
      await store.dispatch('initApp')

      const { user } = store.state.auth
      let nextPage = true
      if (user) {
        const { isAllowed } = store.getters
        if (isAuthPage || !isAllowed(to)) {
          nextPage = '/'
        }
      } else if (!isAuthPage) {
        nextPage = `/${PAGE.login}`
      }
      next(nextPage)
    } catch (e) {
      next(isAuthPage ? true : PAGE.login)
    }

    Loading.hide()
  })
}

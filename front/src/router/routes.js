import capitalize from 'lodash/capitalize'

export const PAGE = {
  login: 'login',
  profile: 'profile',
  recover: 'recover',
  reports: 'reports',
  notify: 'notify',
  users: 'users'
}

const authPage = (name) => {
  const component = capitalize(name)

  return {
    path: `/${name}`,
    component: () => import('layouts/Guest'),
    children: [
      { name, path: '', component: () => import(`pages/${component}.vue`) }
    ]
  }
}

const routes = [
  authPage(PAGE.login),
  authPage(PAGE.recover),
  {
    path: '/',
    component: () => import('layouts/Logged.vue'),
    children: [
      { path: PAGE.notify, component: () => import('pages/Notify.vue') },
      { path: PAGE.profile, component: () => import('pages/Profile.vue') },
      { path: PAGE.reports, component: () => import('pages/Reports.vue') },
      { path: PAGE.users, component: () => import('pages/Users.vue'), meta: { is_admin: true } },
      { path: '', component: () => import('pages/Index.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes

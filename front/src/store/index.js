import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'
import auth from './modules/auth'
import companies from './modules/companies'
import projects from './modules/projects'
import users from './modules/users'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export const store = new Vuex.Store({
  modules: {
    auth,
    companies,
    projects,
    users
  },
  getters: {
    isAllowed: ({ auth: { user: me } }) => ({ meta }) => {
      if (meta && meta.is_admin) {
        return !!me.is_admin
      }
      return true
    }
  },
  actions: {
    initApp: ({ dispatch }) => axios.all([
      dispatch('auth/load').then(() => dispatch('users/load')),
      dispatch('companies/load'),
      dispatch('projects/load')
    ])
  }
})

export default function (/* { ssrContext } */) {
  return store
}

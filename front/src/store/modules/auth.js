import { axios } from 'src/plugins/axios'
import sha1 from 'crypto-js/sha1'
import { LocalStorage, SessionStorage } from 'quasar'

const USER_KEY = 'user'

export default {
  namespaced: true,
  state: {
    user: null
  },
  mutations: {
    setToken (state, token) {
      axios.defaults.headers.common.authorization = (token ? `Bearer ${token}` : null)
    },
    setUser (state, user) {
      state.user = user
    }
  },
  actions: {
    login: ({ commit, dispatch }, { email, password, remember }) => {
      const params = {
        email,
        password: sha1(password).toString()
      }

      return axios.post('login', params).then(({ data: user }) => {
        if (remember) {
          LocalStorage.set(USER_KEY, user)
        } else {
          SessionStorage.set(USER_KEY, user)
        }
        return dispatch('load')
      })
    },
    changePassword: (context, password) => axios.post('change-password', { password: sha1(password).toString() }),
    recover: (context, form) => axios.post('reset-password', form),
    logout: ({ dispatch }) => {
      LocalStorage.remove(USER_KEY)
      SessionStorage.remove(USER_KEY)

      return dispatch('load')
    },
    load: async ({ commit }) => {
      let user = LocalStorage.get.item(USER_KEY) || SessionStorage.get.item(USER_KEY) || null
      commit('setToken', (user ? user.api_token : null))
      commit('setUser', user)

      return user
    }
  }
}

import { axios } from 'src/plugins/axios'

export default {
  namespaced: true,
  state: {
    data: []
  },
  getters: {
    find: ({ data }) => id => data.find(c => c.id === id)
  },
  actions: {
    load: ({ state }) => {
      const { data } = state
      if (data.length) {
        return Promise.resolve(data)
      }

      return axios.get('companies').then(({ data }) => {
        state.data = data
        return data
      })
    }
  }
}

import { axios } from 'src/plugins/axios'

export default {
  namespaced: true,
  state: {
    data: []
  },
  getters: {
    find: ({ data }) => id => data.find(project => project.id === id)
  },
  actions: {
    load: ({ state }) => {
      const { data } = state
      if (data.length) {
        return Promise.resolve(data)
      }

      return axios.get('projects').then(({ data }) => {
        state.data = data
        return data
      })
    }
  }
}

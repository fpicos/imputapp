import { axios } from 'src/plugins/axios'
import { store } from 'src/store'

export default {
  namespaced: true,
  state: {
    data: []
  },
  getters: {
    getName: ({ data }) => (id) => {
      const { name, surname } = data.find(user => user.id === id)
      return `${name} ${surname}`
    },
    find: ({ data }) => id => data.find(user => user.id === id)
  },
  actions: {
    load: ({ state }) => {
      const { user } = store.state.auth
      if (user) {
        return axios.get('users').then(({ data }) => {
          state.data = data
          return data
        })
      }

      return Promise.resolve(state.data)
    }
  }
}

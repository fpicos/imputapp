import moment from 'moment'

const SECOND = 1000
const MINUTE = 60 * SECOND
const HOUR = 60 * MINUTE
const DAY = 24 * HOUR

export const TimeConstants = {
  DAY,
  HOUR,
  MINUTE,
  SECOND
}

export const getDate = datetime => datetime && moment(datetime).toDate()

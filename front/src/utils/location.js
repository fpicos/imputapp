const EARTH_RADIUS = 6378.137

export const diffInKms = (from, to) => {
  const RAD = Math.PI / 180

  const latitude = (from.latitude - to.latitude) * RAD
  const longitude = (from.latitude - to.latitude) * RAD
  const aux = Math.sin(latitude / 2) * Math.sin(latitude / 2) +
    Math.cos(to.latitude * RAD) * Math.cos(from.latitude * RAD) *
    Math.sin(longitude / 2) * Math.sin(longitude / 2)

  return EARTH_RADIUS * 2 * Math.atan(Math.sqrt(aux), Math.sqrt(1 - aux))
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChangePasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Change Password Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for changing user passwords.
    |
    */

    /**
     * @method  changePassword
     * @param   \Collection $request
     * @return  string
     */
    public function changePassword(Request $request)
    {
        $this->validateRequest($request, [
            'next' => 'required|string',
            'confirm' => 'required|string',
        ]);

        if ($request->next !== $request->confirm) abort(Response::HTTP_CONFLICT);

        $user = $request->user();
        $user->password = bcrypt(sha1($request->next));
        $user->save();

        return $user;
    }
}

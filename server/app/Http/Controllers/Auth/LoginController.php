<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application
    |
    */

    /**
     * @method  login
     * @param   \Collection $request
     * @return  \App\User
     */
    public function login(Request $request)
    {
        $this->validateRequest($request, [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) abort(Response::HTTP_UNAUTHORIZED);

        $user = Auth::user();
        if (!$user->api_token)
        {
            $apiToken = str_random(64);
            $user->api_token = $apiToken;
            $user->save();
        }
        $user->makeVisible('api_token');
        return $user;
    }
}

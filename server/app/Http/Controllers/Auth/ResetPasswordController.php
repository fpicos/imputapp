<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

use App\Mail\ResetPasswordMail;
use App\User;

class ResetPasswordController extends Controller
{
/*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails.
    |
    */

    /**
     * @method  resetPassword
     * @param   \Collection $request
     * @return  void
     */
    public function resetPassword(Request $request)
    {
        $this->validateRequest($request, ['email' => 'required|email']);

        $user = User::where($request->only('email'))->first();

        if (!$user) abort(Response::HTTP_UNAUTHORIZED);

        $user->api_token = null;
        $password = str_random(8);
        $user->password = bcrypt(sha1($password));
        $user->save();

        /**
         * @todo cambiar forma de resetear password
         */
        Mail::send(new ResetPasswordMail($password, $user));
    }
}

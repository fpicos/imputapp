<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'admin'])->only('store', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Company::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request, [
            'name' => 'required|string',
            'address' => 'nullable|string',
            'cif' => 'required|string',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
        ]);

        if (Company::where('cif', $request->cif)->first()) abort(Response::HTTP_FORBIDDEN);

        $params = $request->only('name', 'adress', 'cif', 'latitude', 'longitude');
        return Company::create($params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $this->validateRequest($request, [
            'name' => 'required|string',
            'address' => 'nullable|string',
            'cif' => 'required|string',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
        ], $company);

        return $this->updateModel($company, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * This method validates the request (checks the fields)
     *
     * @method  validateRequest()
     * @param   Request $request
     * @param   array $validations
     * @param   object $model
     * @return  void
     */
    protected function validateRequest(Request $request, $validations, $model = null)
    {
        if ($model && $request->id !== $model->id) abort(Response::HTTP_CONFLICT);

        $validate = Validator::make(collect($request)->all(), $validations);
        if (!$validate->passes()) abort(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @method  updateModel()
     * @param   object $model
     * @param   object $request
     * @param   array $properties
     * @return  boolean
     */
    protected function updateModel($model, $request, $properties = [])
    {
        if ($request->id !== $model->id) abort(Response::HTTP_CONFLICT);

        $keys = count($properties) ? collect($properties) : collect($model)->keys();
        $keys->each(function ($key) use (&$model, $request) {
            if ($key != 'id' && collect($request)->keys()->contains($key)) {
                $model->$key = $request->$key;
            }
        });

        $model->save();
        return $model;
    }
}

<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class LocationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api'])->only('store', 'update', 'destroy');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request, [
            'user_id' => 'required|integer|exists:users,id',
            'project_id' => 'required|integer|exists:projects,id',
            'date' => 'required|string',
            'start_at' => 'nullable|string',
            'end_at' => 'nullable|string',
            'description' => 'nullable|string',
        ]);

        if (Auth::id() !== $request->user_id) abort(Response::HTTP_UNAUTHORIZED);

        $start_at = $request->start_at;
        if (strpos($start_at, 'T')) {
            preg_match('/T(.*)\./', $start_at, $start);
            $start_at = $start[1];
        }

        $end_at = $request->end_at;
        if (strpos($end_at, 'T')) {
            preg_match('/T(.*)\./', $end_at, $end);
            $end_at = $end[1];
        }

        return Auth::user()->locations()->create([
            'user_id' => $request->user_id,
            'project_id' => $request->project_id,
            'date' => Carbon::createFromFormat('d/m/Y', $request->date),
            'start_at' => $start_at,
            'end_at' => $end_at,
            'description' => $request->description,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
    }
}

<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'admin'])->only('store', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Project::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request, [
            'name' => 'required|string',
            'company_id' => 'required|integer|exists:companies,id',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
            'is_global' => 'required|boolean',
        ]);

        $params = $request->only('name', 'company_id', 'latitude', 'longitude', 'is_global');
        return Project::create($params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $this->validateRequest($request, [
            'name' => 'required|string',
            'company_id' => 'required|integer|exists:companies,id',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
            'is_global' => 'required|boolean',
        ], $project);

        return $this->updateModel($project, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
    }
}

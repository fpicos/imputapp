<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use App\UserProject;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('index', 'show');
        $this->middleware(['auth:api', 'admin'])->only('store', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with('locations', 'projects')->get();
        if (!$request->user()->is_admin) {
            $users = $users->filter(function ($user) { return $user->id === Auth::id(); })->values();
        }
        return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request, [
            'name' => 'required|string',
            'surname' => 'nullable|string',
            'email' => 'required|email',
            'is_admin' => 'required|boolean',
        ]);

        if (User::where('email', $request->email)->first()) abort(Response::HTTP_FORBIDDEN);

        $params = $request->only('name', 'surname', 'email', 'is_admin');
        $user = new User($params);
        $user->password = bcrypt(sha1('123456'));
        $user->save();

        $globalProjects = Project::where('is_global', true)->get();
        $globalProjects->each(function ($project) use ($user) {
            $userProject = new UserProject;
            $userProject->user()->associate($user);
            $userProject->project()->associate($project);
            $userProject->save();
        });

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return User::with('locations', 'projects')->find($user->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validateRequest($request, [
            'name' => 'required|string',
            'surname' => 'nullable|string',
            'email' => 'nullable|email',
            'is_admin' => 'required|boolean',
        ], $user);

        return $this->updateModel($user, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->is_admin) abort(Response::HTTP_FORBIDDEN);

        $user->delete();
    }
}

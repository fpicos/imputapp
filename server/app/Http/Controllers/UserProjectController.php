<?php

namespace App\Http\Controllers;

use App\UserProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class UserProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'admin'])->only('store', 'update', 'destroy');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request, [
            'user_id' => 'required|integer|exists:users,id',
            'project_id' => 'required|integer|exists:projects,id',
            'work_days' => 'nullable|array',
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
        ]);

        $params = $request->only('user_id', 'project_id', 'work_days');
        $startDate = null;
        if ($request->start_date) {
            $startDate = Carbon::createFromFormat('Y/m/d H:i:s', $request->start_date);
        }
        $endDate = null;
        if ($request->end_date) {
            $endDate = Carbon::createFromFormat('Y/m/d H:i:s', $request->end_date);
        }

        return UserProject::create(array_merge($params, [
            'start_date' => $startDate,
            'end_date' => $endDate,
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserProject  $userProject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserProject $userProject)
    {
        $this->validateRequest($request, [
            'user_id' => 'required|integer',
            'project_id' => 'required|integer',
            'work_days' => 'nullable|array',
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
        ], $userProject);

        $startDate = $request->start_date;
        if ($startDate) {
            $startDate = new Carbon($startDate);
        }

        $endDate = $request->end_date;
        if ($endDate) {
            $endDate = new Carbon($endDate);
        }

        $userProject->project_id = $request->project_id;
        $userProject->work_days = $request->work_days;
        $userProject->start_date = $startDate;
        $userProject->end_date = $endDate;
        $userProject->save();

        return $userProject;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserProject  $userProject
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserProject $userProject)
    {
        $userProject->delete();
    }
}

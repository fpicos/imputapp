<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $newPassword;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($newPassword, $user)
    {
        $this->newPassword = $newPassword;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.reset_password')
            ->with('name', $this->user->name)
            ->with('password', $this->newPassword)
            ->to($this->user->email)
            ->subject('Reseteo de contraseña');
    }
}

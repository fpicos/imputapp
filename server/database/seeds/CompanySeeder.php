<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\es_ES\Company($faker));
        $faker->addProvider(new Faker\Provider\es_ES\Person($faker));
        $faker->addProvider(new Faker\Provider\es_ES\Address($faker));
        $faker->addProvider(new Faker\Provider\es_ES\Payment($faker));

        $company = App\Company::create([
            'name' => 'Mi empresa',
            'address' => 'Dirección de la empresa',
            'cif' => 'A00000000',
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
        ]);

        foreach (['Vacaciones', 'Festivos', 'Formación', 'Bajas por enfermedad', 'Permisos retribuidos'] as $name) {
            $company->projects()->create([
                'name' => $name,
                'is_global' => true,
            ]);
        }

        foreach (range(1, 19) as $number)
        {
            $company = App\Company::create([
                'name' => $faker->company,
                'address' => $faker->address,
                'cif' => $faker->unique()->vat,
            ]);

            $company->projects()->create([
                'name' => "{$company->name} 001",
                'latitude' => $faker->latitude,
                'longitude' => $faker->longitude,
            ]);
        }
    }
}

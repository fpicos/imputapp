<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\es_ES\Person($faker));
        $faker->addProvider(new Faker\Provider\es_ES\Internet($faker));

        $user = App\User::create([
            'name' => 'Fernando',
            'surname' => 'Picos Moreno',
            'email' => 'admin@imputapp.com',
            'password' => bcrypt(sha1('123456')),
            'is_admin' => true,
        ]);

        foreach (range(1, 49) as $number)
        {
            $user = App\User::create([
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'email' => $faker->unique()->email,
                'password' => bcrypt(sha1('123456'))
            ]);
        }

        App\User::get()->each(function ($user) {
            App\Project::where('is_global', true)->get()->each(function($project) use ($user) {
                $user->projects()->create(['project_id' => $project->id]);
            });
        });
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\LoginController@login');
Route::post('reset-password', 'Auth\ResetPasswordController@resetPassword');

Route::apiResource('companies', 'CompanyController')->only('index');
Route::apiResource('projects', 'ProjectController')->only('index');

Route::middleware('auth:api')->group(function() {
    Route::post('change-password', 'Auth\ChangePasswordController@changePassword');
    Route::apiResource('users', 'UserController')->only('index');
    Route::apiResource('locations', 'LocationController')->only('store', 'update', 'destroy');

    Route::middleware('admin')->group(function() {
        Route::apiResource('companies', 'CompanyController')->only('store', 'update', 'destroy');
        Route::apiResource('projects', 'ProjectController')->only('store', 'update', 'destroy');
        Route::apiResource('user-projects', 'UserProjectController')->only('store', 'update', 'destroy');
        Route::apiResource('users', 'UserController')->only('store', 'update', 'destroy');
    });
});
